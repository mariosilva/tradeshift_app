# [Tradeshift App]

#last update 03:20 - 29/10

Author: Mario Silva - contato@marioeduardo.com

## Installation

1. Clone this repository
1. Install [NodeJS](https://nodejs.org/), either LTS or current.
1. Install the dependencies of this project.
   - `npm install`

## Usage (Local Development)

1. Run `npm start` script in the root of the repository.
   - `npm start`
1. Whenever you modify the source files, the script will rebuild the files, so you're always using the latest version.

## Release & Deployment

1. Run `npm build` to create the build version. The path it will be in ./public
   - `npm build`

## Run Test

1. Run `npm test`
