import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
/* Importing css files */
import "./style/css/main.css";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.getElementById("root"));

serviceWorker.register();
