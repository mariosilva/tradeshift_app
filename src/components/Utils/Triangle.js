export default class {
  //Function to find the type of triangle
  static getType = (a, b, c) => {
    if (a === b && b === c) {
      return "equilateral";
    }
    if (a === b || a === c || b === c) {
      return "isoceles";
    }
    return "scalene";
  };

  //Function to check the side of triangle are valid
  static hasValidSides = (a, b, c) => {
    let triangleSides = [a, b, c];
    if (triangleSides.length !== 3) {
      console.error("a triangle must have 3 sides");
      return false;
    }
    // All sides of a triangle must be bigger than 0
    if (triangleSides.filter(side => parseFloat(side) <= 0).length > 0) {
      console.error("a triangle side must be bigger than 0");
      return false;
    }
    return true;
  };
}
