import React, { Component } from "react";

/* Import Components */
import Input from "./Input";
import Button from "./Button";

/* Import Function */
import Triangle from "../Utils/Triangle";

class Form extends Component {
  state = {
    typeTriangle: undefined
  };

  constructor(props) {
    super(props);

    this.state = {
      newTriangle: {
        sideA: "",
        sideB: "",
        sideC: ""
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(prevState => ({
      newTriangle: {
        ...prevState.newTriangle,
        [name]: value
      }
    }));
  }

  handleSubmit(e) {
    e.preventDefault();
    const sideA = this.state.newTriangle.sideA;
    const sideB = this.state.newTriangle.sideB;
    const sideC = this.state.newTriangle.sideC;
    if (sideA && sideB && sideC) {
      if (Triangle.hasValidSides(sideA, sideB, sideC)) {
        this.setState({ typeTriangle: Triangle.getType(sideA, sideB, sideC) });
      } else {
        this.setState({ typeTriangle: "Problem with sum of values" });
      }
    } else {
      this.setState({ typeTriangle: "Insert values in all fields" });
    }
  }

  render() {
    return (
      <React.Fragment>
        <form
          data-ts="Form"
          className="tradeshift__block--6"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <Input
              type={"number"}
              title={"Insert Side A"}
              name={"sideA"}
              value={this.state.newTriangle.sideA}
              placeholder={"Insert Side A"}
              onChange={this.handleChange}
            />
            <Input
              type={"number"}
              title={"Insert Side B"}
              name={"sideB"}
              value={this.state.newTriangle.sideB}
              placeholder={"Insert Side B"}
              onChange={this.handleChange}
            />
            <Input
              type={"number"}
              title={"Insert Side C"}
              name={"sideC"}
              value={this.state.newTriangle.sideC}
              placeholder={"Insert Side C"}
              onChange={this.handleChange}
            />
            <Button />
          </fieldset>
        </form>
        <div className="tradeshift__block--6">{this.state.typeTriangle}</div>
      </React.Fragment>
    );
  }
}

export default Form;
