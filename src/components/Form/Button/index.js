import React, { Component } from "react";

class Button extends Component {
  render() {
    return (
      <button
        data-ts="Button"
        type="submit"
        value="Submit"
        className="ts-primary tradeshift__btn--big"
      >
        <span>Send</span>
      </button>
    );
  }
}

export default Button;
