import React, { Component } from "react";

class Input extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.props.onChange(e);
  }

  render() {
    return (
      <label htmlFor={this.props.name}>
        <input
          id={this.props.name}
          name={this.props.name}
          type={this.props.type}
          value={this.props.value}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}
          required
        />
      </label>
    );
  }
}

export default Input;
