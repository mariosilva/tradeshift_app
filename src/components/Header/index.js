import React, { Component } from "react";

class Header extends Component {
  static update({ visible, title, buttons }) {
    window.ts.ui.ready(() => {
      const bar = window.ts.ui.TopBar;
      if (visible) {
        bar.title(title);
        bar.buttons(buttons);
        bar.show();
      } else {
        bar.hide();
      }
    });
  }

  componentDidMount() {
    Header.update(this.props);
  }
  render() {
    return <header data-ts="TopBar" className="tradeshift__header" />;
  }
}

Header.defaultProps = {
  buttons: [],
  title: "Triangle Challenge",
  visible: true
};

export default Header;
