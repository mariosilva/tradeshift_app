import React, { Component } from "react";
import Form from "../Form";

class Main extends Component {
  render() {
    return (
      <main data-ts="Main" className="tradeshift__main ts-main">
        <div data-ts="MainContent">
          <div className="tradeshift__content">
            <Form />
          </div>
        </div>
      </main>
    );
  }
}

export default Main;
