import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Triangle from "./components/Utils/Triangle";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("valid parameters for create a triangle", () => {
  Triangle.hasValidSides(["10", "10", "3"]);
});
