import React, { Component } from "react";
import Header from "./components/Header/";
import Main from "./components/Main/";

class App extends Component {
  state = {
    loading: true
  };

  // Remove loading screen after completed loading app
  componentDidMount() {
     setTimeout(
      () =>
        this.setState({ loading: false }, () => {
          // Just to simulate how is going to work loading the app
          let screenLoading = document.getElementById("loadingApp");
          screenLoading.parentNode.removeChild(screenLoading);
        }),
      3000
    );
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return null;
    }

    return (
      <React.Fragment>
        <Header />
        <Main />
      </React.Fragment>
    );
  }
}

export default App;
